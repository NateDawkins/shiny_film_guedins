library(shiny)
library(shinydashboard)
library(shinyjs)
library(shinyWidgets)
library(httr)
library(purrr)
library(readxl)
# library(tidyverse)

# API
api_key <- "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIwZTc0MTUzY2E1OTBjN2UyOGE5ZWQ2YjllNzhjMjE2MSIsInN1YiI6IjYxYWNkMjBmYTg0YTQ3MDA2MTk5ZjBhNyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.jwhjM-uXAm6mHutLUzuc6S3uz8_bMl6esy-jY_Mmkfk"

donnees <- readxl::read_xlsx("www/Notes_Films_Guedins.xlsx")

choices_P <- c("Paul", "Nathan", "Matthieu", "Léonie", "Lucas", "Bastien", "Emilie", "Andreia", "Anthony", "Adam", "Maxime", "Gwendal")

# Fonction pour récupérer les suggestions de films en fonction du texte de recherche
getMovieSuggestions <- function(searchText) {
  url <- "https://api.themoviedb.org/3/search/movie"
  queryMovie <- list(
    query = searchText,
    include_adult = "true",
    language = "fr",
    page = "1"
  )
  
  response <- GET(url, add_headers(Authorization = paste0("Bearer ", api_key)),
                  query = queryMovie,
                  content_type = "application/octet-stream",
                  accept = "application/json")
  
  
  if(status_code(response) == 200){
    data <- content(response, "parsed")
    movies <- data$results
    movieTitles <- movies %>% map(pluck, "title") %>% unique()
    return(movieTitles)
  } else {
    return(character())
  }
}


# UI ####
ui <- dashboardPage(
  dashboardHeader(titleWidth = 358, title = "Données Films Guedins"),
  sidebar <- dashboardSidebar(width = 400, sidebarMenu(menuItem(text = div(HTML("&nbsp; Noter un film/série"), class = 'titre_sidebar'), tabName = "note_film"),
                                                       menuItem(text = div(HTML("&nbsp; Graphiques"), class = 'titre_sidebar'), tabName = "graphique"),
                                                       div(tags$img(height = 200, src = 'logo_guedin.svg'), class = 'img'))
                              ),
  body <- dashboardBody(
    useShinyjs(),
    
    tags$head(
      tags$link(rel = "stylesheet", type = "text/css", href = "styles.css"),
      tags$style("@import url(https://use.fontawesome.com/releases/v6.1.2/css/all.css);")
      # tags$link(rel = "stylesheet", type = "text/css", href = "font_awesome_css.css"),
      # tags$script(src = "scriptJS.js")
    ),
    
    tabItems(tabItem(tabName = "note_film",
                     
                     column(width = 12, div(HTML("Note un film"), class = 'titre_section')),
                     
                     column(width = 4, pickerInput(inputId = "PI_pseudo", label = "T'es qui ?",
                                                   choices = choices_P, options = pickerOptions(title = "Mais t'es qui ?"))),
                     
                     column(width = 4, pickerInput(inputId = "PI_nom_film", label = "Nom du film", multiple = FALSE, choices = c(""),
                                                   options = pickerOptions(title = "Sélectionne le film", liveSearch = TRUE))),
                     
                     column(width = 4, sliderInput(inputId = "SI_note_film", label = "Note", min = 0, max = 20, value = 10, step = 0.5)),
                     
                     column(width = 12, div(textAreaInput("TA_Commentaire_film",
                                                          "Votre commentaire :",
                                                          rows = 5, width = "100%"), class = 'commentaire')),
                     
                     column(width = 12, uiOutput("UI_button_confirm_film"))
                     )
             )
  )
)

# server ####
server <- function(input, output, session) {
  
  # Observer pour mettre à jour les suggestions de films en fonction du texte de recherche
  observeEvent(input$TA_Commentaire_film, {
    
    print(input$TA_Commentaire_film)
    searchText <- input$TA_Commentaire_film
    suggestions <- getMovieSuggestions(searchText)
    updatePickerInput(session, "PI_nom_film", choices = suggestions)
  })
  
  output$UI_button_confirm_film <- renderUI({
    req(input$PI_nom_film != "")
    
    div(actionBttn(inputId = "button_validation_film", label = "Confirmer", color = "success", size = "md"), class = 'button')
  })
  
}

# Run the application 
shinyApp(ui = ui, server = server)
